package com.fj.yamba.fragment;

import android.app.ListFragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.fj.yamba.R;
import com.fj.yamba.activity.DetailsActivity;
import com.fj.yamba.database.StatusContract;

public class TimelineFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor>
{
    private static final String TAG = TimelineFragment.class.getSimpleName();
    private static final String[] FROM = {StatusContract.Column.USER, StatusContract.Column.MESSAGE, StatusContract.Column.CREATED_AT};
    private static final int[] TO = {R.id.list_item_text_user, R.id.list_item_text_message, R.id.list_item_text_created_at};
    private static final int LOADER_ID = 42;

    private SimpleCursorAdapter mAdapter;

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        setEmptyText("Loading data...");

        mAdapter = new SimpleCursorAdapter(getActivity(), R.layout.list_item, null, FROM, TO, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        mAdapter.setViewBinder(new TimelineViewBinder());

        setListAdapter(mAdapter);

        Log.d(TAG, "b4 break");
        getLoaderManager().initLoader(LOADER_ID, null, this);
        Log.d(TAG, "after break");
    }

    private static final class TimelineViewBinder implements SimpleCursorAdapter.ViewBinder
    {

        @Override
        public boolean setViewValue(View view, Cursor cursor, int columnIndex)
        {
            long timestamp;
            // Custom binding
            switch (view.getId())
            {
                case R.id.list_item_text_created_at:
                {
                    timestamp = cursor.getLong(columnIndex);
                    CharSequence relTime = DateUtils.getRelativeTimeSpanString(timestamp);
                    ((TextView) view).setText(relTime);
                    return true;
                }
                default:
                {
                    return false;
                }
            }
        }
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id)
    {
        DetailsFragment fragment = (DetailsFragment) getFragmentManager().findFragmentById(R.id.fragment_details);

        if (fragment != null && fragment.isVisible())
        {
            fragment.updateView(id);
        }
        else
        {
            startActivity(new Intent(getActivity(), DetailsActivity.class).putExtra(StatusContract.Column.ID, id));
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        if (id != LOADER_ID)
        {
            return null;
        }
        Log.d(TAG, "onCreateLoader");

        Loader<Cursor> loader = new CursorLoader(getActivity(), StatusContract.CONTENT_URI, null, null, null, StatusContract.DEFAULT_SORT);
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        Log.d(TAG, "load finished");
        DetailsFragment fragment = (DetailsFragment) getFragmentManager().findFragmentById(R.id.fragment_details);

        if (fragment != null && fragment.isVisible() && data.getCount() == 0)
        {
            fragment.updateView(-1);
            Toast.makeText(getActivity(), "No data", Toast.LENGTH_LONG).show();
        }

        Log.d(TAG, "onLoadFinished with cursor: " + data.getCount());
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader)
    {
        Log.d(TAG, "loader reset");
        mAdapter.swapCursor(null);
    }
}
