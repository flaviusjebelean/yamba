package com.fj.yamba.services;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.fj.yamba.database.DBHelper;
import com.fj.yamba.database.StatusContract;
import com.fj.yamba.database.StatusProvider;
import com.marakana.android.yamba.clientlib.YambaClient;
import com.marakana.android.yamba.clientlib.YambaClientException;

import java.util.List;

public class RefreshService extends IntentService
{
    private static final String TAG = "RefreshService";

    public RefreshService()
    {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        final String username = preferences.getString("username", "");
        final String password = preferences.getString("password", "");
        final String api = preferences.getString("apiRoot","");

        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password))
        {
            Toast.makeText(this, "Please update your username, password or API Root", Toast.LENGTH_LONG).show();
            return;
        }

        Log.d(TAG, "onStarted");

        DBHelper dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        YambaClient cloud = new YambaClient(username, password, api);
        try
        {
            int count = 0;
            List<YambaClient.Status> timeline = cloud.getTimeline(20);
            for (YambaClient.Status status : timeline)
            {
                contentValues.clear();
                contentValues.put(StatusContract.Column.ID, status.getId());
                contentValues.put(StatusContract.Column.USER, status.getUser());
                contentValues.put(StatusContract.Column.MESSAGE, status.getMessage());
                contentValues.put(StatusContract.Column.CREATED_AT, status.getCreatedAt().getTime());
                //db.insertWithOnConflict(StatusContract.TABLE, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);

                Uri uri = getContentResolver().insert(StatusContract.CONTENT_URI, contentValues);
                if (uri != null)
                {
                    count++;
                    Log.d(TAG, String.format("Adding %s: %s", status.getUser(), status.getMessage()));
                }
            }

            if (count > 0)
            {
                sendBroadcast(new Intent("com.fj.yamba.action.NEW_STATUSES").putExtra("count", count));
            }
        }
        catch (YambaClientException e)
        {
            Log.d(TAG, "Failed to fetch the timeline");
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        Log.d(TAG, "onCreated");
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Log.d(TAG, "onDestroyed");
    }
}
