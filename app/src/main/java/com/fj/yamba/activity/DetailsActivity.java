package com.fj.yamba.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.fj.yamba.fragment.DetailsFragment;

public class DetailsActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null)
        {
            DetailsFragment fragment = new DetailsFragment();
            this.getFragmentManager().beginTransaction().add(
                    android.R.id.content, fragment, fragment.getClass().getSimpleName()).commit();
        }
    }
}
