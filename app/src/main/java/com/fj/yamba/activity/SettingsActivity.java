package com.fj.yamba.activity;

import android.app.Activity;
import android.os.Bundle;

import com.fj.yamba.fragment.SettingsFragment;

public class SettingsActivity extends Activity
{
    @Override
    protected void onCreate(Bundle savedInstanceBundle)
    {
        super.onCreate(savedInstanceBundle);

        //Check whether this activity was created before
        if (savedInstanceBundle == null)
        {
            SettingsFragment fragment = new SettingsFragment();
            this.getFragmentManager().beginTransaction().add(
                    android.R.id.content, fragment, fragment.getClass().getSimpleName()).commit();
        }
    }
}
